/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
} from 'react-native';

import {Router, Scene, Stack} from 'react-native-router-flux';

import Rotas from './src/components/Rotas'

export default class CaraCoroa extends Component {
  render() {
    return (
      <Rotas/>
    );
  }
}

AppRegistry.registerComponent('CaraCoroa', () => CaraCoroa);
