import React, { Component } from 'react';
import {
	StyleSheet,
    Text,
    Image,
    View
} from 'react-native';

const cara = require('../img/moeda_cara.png');
const coroa = require('../img/moeda_coroa.png');

export default class Resultado extends Component {
    constructor(props){
        super(props);

        this.state = { resultado: '' }
        
    }

    componentDidMount(){
        const numAleatorio = Math.floor(Math.random() * 2);
        let cara_coroa = '';
        if(numAleatorio === 0){
            cara_coroa = 'cara'
        }
        else{
            cara_coroa = 'coroa'
        }

        this.setState( { resultado: cara_coroa } )
    }
	render(){
        if(this.state.resultado === 'cara'){
            return(
                <View style={ estilo.resultado }>
                    <Image source={ cara } />
                </View>
            )
        }
        return(
            <View style={ estilo.resultado }>
                <Image source={ coroa } />
            </View>
        )
	}
}

const estilo = StyleSheet.create({
    resultado: {
        flex: 1,
        backgroundColor: '#61bd8c',
        justifyContent: 'center',
        alignItems: 'center',
    }
})