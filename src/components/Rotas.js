import React, { Component } from 'react';
import {
  AppRegistry,
} from 'react-native';

import {Router, Scene, Stack} from 'react-native-router-flux';

import Principal from './components/Principal'
import SobreJogo from './components/SobreJogo';
import OutrosJogos from './components/OutrosJogos';
import Resultado from './components/Resultado';

const rotas = () => (
        <Router>
        <Stack>
          <Scene key='principal' titleStyle={{alignSelf: 'center'}} component={Principal} initial title="Cara ou Coroa" />
          <Scene key='sobrejogo' titleStyle={{alignSelf: 'center'}} title="Sobre o Jogo" component={SobreJogo} />
          <Scene key='outrosjogos' titleStyle={{alignSelf: 'center'}} title="Outros Jogos" component={OutrosJogos} />
          <Scene key='resultado' titleStyle={{alignSelf: 'center'}} title="Resultado" component={Resultado} />
        </Stack>
      </Router>
    )

export default Rotas