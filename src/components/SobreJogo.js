import React, { Component } from 'react';
import {
	StyleSheet,
	Text,
} from 'react-native';

export default class SobreJogo extends Component {
	render(){
		return(
			<Text style={ {flex: 1, backgroundColor: '#61bd8c'} }>Sobre o jogo</Text>
		)
	}
}